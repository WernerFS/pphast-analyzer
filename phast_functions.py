''' PHAST Functions
This module provides the back bone functions for the cmd prompt.
Most functions rely on multiprocessing for improved performance.
'''

import pathlib, re, joblib, os, pandas, numba
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib.widgets import RadioButtons
from functools import partial
from p_tqdm import p_umap
from math import ceil
import numpy as np
import lmfit
from mpl_toolkits.axes_grid1 import make_axes_locatable

PLOG = 0.2784645427610738

def align_channel(data: list) -> np.ndarray:
    ''' Aligns the data comming from two interleaved ADCs.
    Performs a mean difference to minimize the offset between the ADCs:
        diff = mean(even) - mean(odd)
    The difference is subtracted from each value of the even ADC
    :param list data: List of two interleaved ADC 
    
    :returns: Aligned values
    :rtype: np.ndarray
    '''
    channel1 = data[0::2]
    channel2 = data[1::2]
    offset1 = np.mean(channel1)
    offset2 = np.mean(channel2)
    d = offset1 - offset2 # mean(even) - mean(odd)
    data[::2] = [x - d for x in channel1]
    data[1::2] = channel2
    data = np.asarray(data)
    return data

def stats(data: list) -> list:
    ''' Calculates mean and standard deviation
    Returns
    -------
        mean and standrad deviation 
    '''
    return [np.mean(data), np.std(data)]

def standardize(data: list, stat: list) -> np.ndarray:
    ''' Not real standardization but a modified 
    value to keep the mean of the signal
    Returns
    -------
    numpy array
        data values pseudo-standardized
    '''
    if stat[1] != 0:
        return (data - np.amin(data) + 1) / stat[1]
    else:
        return data

@numba.njit
def barycenter(data: list) -> float:
    ''' Calculates de barycenter or a 32 value array
    Returns
    -------
    float
        barycenter value of data
    '''
    data_sum = np.sum(data)
    size = len(data)
    if data_sum != 0:
        return np.sum(data * np.arange(0, size)) / data_sum
    else:
        return -1

def parser_worker1(lines: list) -> list:
    ''' Worker for multiprocessing 
    Returns
    -------
    list
        that holds the information of the pulse
    '''
    pixels = []
    for line in lines:
        if line[0].isdigit():
            l = list(re.sub(r'=*#*>*,*\.*','',line).split(' '))
            l = [x for x in l if x.strip()]
            pixel_offset = l.index('is')
            data = align_channel(list(map(float, l[0 : l.index('Run')]))) 
            stat = stats(data)
            std_data = standardize(data, stat)
            max_val = np.amax(data) - np.mean(data[:4])
            l = [float(l[l.index('Run') + 1]),
                float(l[l.index('Spill') + 1]),
                float(l[l.index('Event-in-Spill') + 1]),
                float(l[pixel_offset + 1]),
                float(l[pixel_offset + 2]),
                float(barycenter(std_data)),
                float(max_val)]
            l = np.concatenate((l, stat, data, std_data))
            pixels.append(l)
    return pixels

def parser_worker2(lines: list) -> list:
    ''' Worker for multiprocessing 
    Returns
    -------
    list
        that holds the information of the pulse
    '''
    pixels = []
    for line in lines:
        if line[0:2] == 'Fl':
            l = list(re.sub(r'=*#*>*,*','',line).split(' '))
            l = [x for x in l if x.strip()]
        elif line[0].isdigit():
            pixel_offset = l.index('is')
            data = align_channel(list(map(float, line.rstrip().split(' '))))
            stat = stats(data)
            std_data = standardize(data, stat)
            max_val = np.amax(data) - np.mean(data[:4])
            l = [float(l[l.index('event') + 1]),
                float(l[l.index('Spill') + 1]),
                float(l[l.index('Event-in-Spill') + 1]),
                float(l[pixel_offset + 1]),
                float(l[pixel_offset + 2]),
                float(barycenter(std_data)),
                float(max_val)]
            l = np.concatenate((l, stat, data, std_data))
            pixels.append(l)
    return pixels 
    
def create_runs(filename: pathlib.Path, style: int) -> list:
    ''' Spawns process to process file data and create the
    data list to be analysed 
    Returns
    -------
    list
        that holds the information of the pulses
    '''
    pixels = []
    chunk_size = 24 * os.cpu_count()
    with open(filename, 'r') as f:
        lines = f.readlines()
        if style == 0:
            pixels = p_umap(parser_worker1, 
                list(lines[line : line + chunk_size] 
                for line in range(0, len(lines), chunk_size)))
        else:
            pixels = p_umap(parser_worker2, 
                list(lines[line : line + chunk_size] 
                for line in range(0, len(lines), chunk_size)))
    return [item for sublist in pixels for item in sublist]

def process_file(args: str, style: int, cache: bool) -> list:
    ''' Validates the filepath
    Returns
    -------
    list
        that holds the information of the pulse
    '''
    filename = pathlib.Path(args)
    if filename.exists():
        print('Ready to process: ' + filename.name)
        runs = create_runs(filename, style)
        if cache:
            create_cache(args, runs)
        return runs
    else:
        return 

def create_cache(args: pathlib.Path, runs: list):
    ''' Dumps the python processed data to a binary file.'''
    if not os.path.exists(args + '.cache'):
        with open(args + '.cache', 'wb') as cache_file:
            print('Caching data for future analysis')
            joblib.dump(runs, cache_file, compress=True)

def load_cache(args: str) -> list:
    ''' Validates the filepath and loads the cache build with joblib
    Returns
    -------
    list
        that holds the information of the pulses
    '''
    if os.path.exists(args):
        print('Loading: ' + args)
        json_file = open(args, 'rb')
        try:
            return joblib.load(json_file)
        except Exception as err:
            print('Invalid Cache file: ' + str(err))
            return
    else:
        print('missing file to process')
        return

def filter_worker(data: list, args) -> list:
    ''' Multiprocessing worker to filter data based on barycenter and mean difference threshold
    Returns
    -------
    list
        that holds the information of the pulses filtered according the parameters
    '''
    size = (len(data[0]) - 9) // 2
    filtered = []
    for l in data:
        filters= [True]*4
        if args.bary:
            if not(args.bary[0] < l[5] and l[5] < args.bary[1]):
                filters[0]= False
        if args.mean:
            if not(mean_rule(l[9:9 + size], args.mean)):
                filters[1]= False
        if args.exclude:
            if (l[3],l[4]) in args.exclude:
                filters[2]= False
        if args.max_values:
            if l[6] < args.max_values:
                filters[3]= False
        if all(filters):
            filtered.append(l)
    return filtered

def filter(data: list, args: list) -> list:
    ''' Multiprocessing manager, spawns filtering workers
    Returns
    -------
    list
        that holds the information of the pulse
    '''
    processors = os.cpu_count()
    filtered = []
    chunk_size = 24 * processors if len(data)/processors > 5000 else \
            ceil(len(data)/processors)
    filtered = p_umap(partial(filter_worker, args = args),
        list(data[line : line + chunk_size] 
        for line in range(0, len(data) , chunk_size)))
    return [item for sublist in filtered for item in sublist]

def mean_rule(data: list, thrs: float = 0) -> bool:
    ''' Multiprocessing manager, spawns filtering workers
    Returns
    -------
    list
        that holds the information of the filtered pulses of all workers
    '''
    size = len(data)
    quarter = size // 4
    a = (np.sum(data[quarter : 3 * quarter]))/ (size / 2)
    b = (np.sum(data[0 : quarter]) + np.sum(data[3 * quarter : size]))/ (size / 2)
    return True if a - b >= thrs else False

def search_worker(data: list, args: list) -> list:
    ''' Multiprocessing worker to search data based on run, spill, event and x y value
    Returns
    -------
    list
        that holds the information of the pulses found according the parameters
    '''
    found = []
    for l in data:
        filters = [True]*5
        if args.run:
            if args.run != l[0]:
                filters[0]= False
        if args.spill:
            if args.spill != l[1]:
                filters[1]= False
        if args.event:
            if args.event != l[2]:
                filters[2]= False
        if args.cell:
            if not (args.cell == l[3:5]).all():
                filters[3]= False
        if args.segment:
            if not (args.segment[0] <= l[3] and  args.segment[1] <= l[4] and args.segment[2] >= l[3] and  args.segment[3] >= l[4]):
                filters[4]= False
        if all(filters):
            found.append(l)
    return found

def search(data: list, args: list) -> list:
    ''' Multiprocessing manager spawn search workers
    Returns
    -------
    list
        that holds the information of the pulses found by all workers
    '''
    processors = os.cpu_count()
    found = []
    chunk_size = 24 * processors if len(data)/processors > 5000 else \
        ceil(len(data)/processors)
    found = p_umap(partial(search_worker, args = args), 
        list(data[line : line + chunk_size] 
        for line in range(0, len(data), chunk_size)))
    return [item for sublist in found for item in sublist]

@numba.njit
def math_model(x, N: float, t0: float, amp: float, tau: float, offset: float, k: float,
         dt: float):
    crrcn = np.zeros(np.shape(x))
    norm = (-2 * tau**2 + np.exp((2 *(tau + tau * PLOG))/ tau)*(2 * tau**2 - 4 * tau *(tau + tau * PLOG) \
        + 4*(tau + tau * PLOG)**2))/ np.exp((4 *(tau + tau * PLOG))/ tau)
    for t in x:
        if t < t0:
            crrcn[t] = offset
        elif t0 <= t and t < t0 + dt:
            crrcn[t] = offset + amp / norm * ( \
                np.exp((-2 * t + t0)/tau)*(-2 * np.exp(t0/tau)* tau**2 + np.exp(t / tau)*(t**2 + t0**2 + 2 * t0 * tau + 2 * tau**2 - 2 * t * (t0 + tau))))
        elif t0 + dt <= t: 
            crrcn[t] = offset + amp /norm * ( \
                np.exp((-2 * t + t0)/tau)*(-2 * np.exp(t0/tau)* tau**2 + np.exp(t / tau)*(t**2 + t0**2 + 2 * t0 * tau + 2 * tau**2 - 2 * t * (t0 + tau))) + \
                k * (np.exp((-2 * t + (t0 + dt))/tau)*(-2 * np.exp((t0 + dt)/tau)* tau**2 + np.exp(t / tau)*(t**2 + (t0 + dt)**2 + 2 * (t0 + dt) * tau + 2 * tau**2 - 2 * t * ((t0 + dt) + tau)))))
    return crrcn

@numba.njit
def crrcn(x, N: float, t0: float, amp: float, tau: float, offset: float, k: float, 
        dt: float):
    crrcn = np.zeros(np.shape(x))
    for t in x:
        if t < t0:
            crrcn[t] = offset
        elif t0 <= t and t < t0 + dt:
            crrcn[t] = offset + amp * (np.exp(N) / (N ** N)) * ((t - t0) / tau) ** N * np.exp( - (t - t0) / tau) 
        elif t0 + dt <= t: 
            crrcn[t] = offset + amp * (np.exp(N) / (N ** N)) * ((t - t0) / tau) ** N * np.exp( - (t - t0) / tau) \
                + k * amp * (np.exp(N) / (N ** N)) * ((t - (t0 + dt)) / tau) ** N * np.exp( - (t - (t0+ dt)) / tau)
    return crrcn

@numba.njit
def t0_alt(d, max):
    halfmax = max/2 + np.mean(d[:4])
    x0 = (np.abs(d[:1 if np.argmax(d) == 0 else np.argmax(d)] - halfmax)).argmin()
    x1 = x0 + 1
    if (d[x1] - d[x0]) != 0:
        return (halfmax - d[x1])/(d[x1] - d[x0]) + x1
    else:
        return -1

def fitting_worker(data: list, settings: list) -> list:
    ''' Multiprocessing worker to fit data to the crrcn math model with params as initial values
    Returns
    -------
    list
        that holds the information of the pulses fitted according the parameters
    '''
    model, params, method, chisqr = settings
    size = (len(data[0]) - 9) // 2
    x = np.arange(0,size)
    results = []; residuals_ac = []
    for d in data: # fitting on real data, to use standardized change index, see format
        try:
            result = model.fit(d[9:size + 9], params= params, x=x, method= method)
            residuals_ac = auto_correlation(result.residual)
            if chisqr is not None and type(chisqr) == int:
                if result.chisqr < chisqr: # perform a Chisqr filtering
                    results.append([result, residuals_ac, d[:9], t0_alt(d[9:size + 9], d[6])])
            else:
                results.append([result, residuals_ac, d[:9], t0_alt(d[9:size + 9], d[6])])
        except Exception as err:
            print(err)
            pass
    results.sort(reverse= True, key = lambda x:x[0].chisqr)
    return results  

def make_parameters(model, params: dict, bounds: dict = None, mask: dict = None):
    pars = model.make_params()
    for (k,v), (_,v2), (_, v3) in zip(params.items(),bounds.items(), mask.items()):
        pars.add(name=k, value=v, vary=v3, min=v2[0], max=v2[1])
    return pars

def fitting(data: list, parameters: dict, bounds: dict, mask: dict) -> list:
    ''' Multiprocessing manager spawn fit workers
    Returns
    -------
    A list with the following format:
        [params, residuals, residuals_ac, data, chisqr]
    '''
    model = lmfit.Model(math_model)
    method = parameters.pop('method', 'leastsq')
    params = make_parameters(model, parameters, bounds, mask)
    settings = [model, params, method, parameters.pop('chisqr', None)]
    processors = os.cpu_count()
    size = (len(data[0]) - 9) // 2
    x = np.arange(0,size)
    try:
        model.fit(data[0][9:size + 9], params= params, x=x, method= method)
    except Exception as err:
        print(err)
        return
    x1 = 5000
    if len(data) <= processors:
        chunk_size = ceil(len(data)/processors)
    elif processors < len(data) and len(data) <= x1:
        chunk_size = 1 + (len(data) - processors)*(450 - 1)//(x1 - processors)
    elif x1 < len(data):
        chunk_size = 450
    if method == 'emcee':
        fit = fitting_worker(data= data, settings= settings)
    else:
        fit = p_umap(partial(fitting_worker, settings = settings), 
            list(data[line : line + chunk_size] 
            for line in range(0, len(data), chunk_size)))
    return [item for sublist in fit for item in sublist], model

def get_plot(fig, pixel: list):
    ''' Creates axis of data and pseudo standardized data
    Returns
    -------
    axis
        mathplotlib axis for plotting
    '''
    size = (len(pixel) - 9)// 2

    x = np.arange(0, size)
    ax = fig.add_subplot('211')
    ax.plot(x, pixel[9:9 + size], 'bo', linewidth = 1.0)
    ax.grid(True)
    ax.set_title(f"Run: {pixel[0]} Spill: {pixel[1]}  Event: {pixel[2]} \
        Cell: {pixel[3]} {pixel[4]}")
    ax.set_ylabel('ADC Units')
    ax.set_xlabel('Sample Time Periods')
    rows = ['Barycenter', 'Peak Amplitude','Mean', 'Standard Deviation']
    ax1 = fig.add_subplot('212')
    ax1.table(cellText= list(zip(rows,pixel[5:9])), loc='center')
    ax1.axis('off')
    ax1.axis('tight')
    return fig

def create_image(event: list, show: bool = False):
    ''' Legacy function without support of new data format
    Recreates the image of the calorimeter with the x y of each event
    Returns
    -------
    image
        matplotlib image object
    '''
    depth = len(event[0].data)
    image = np.zeros((64, 48, depth))
    for pixel in event:
        image[pixel.x][pixel.y] = pixel.data
    if show == True:
        plt.imshow(image[:][:][0], interpolation='nearest')
        plt.show()
    return image

def event_movie(event: list, outputFilename: str = None, show: bool = False):
    ''' Legacy function without support of new data format
    Recreates the event in the calorimeter
    Returns
    -------
    '''
    scene = create_image(event)
    im = plt.imshow(scene[:][:][0])
    def animate(i):
        im.set_array(scene[:][:][i])
        return im,
    fig = plt.figure() 
    ani = animation.FuncAnimation(fig, animate, frames = range(32), interval = 200, 
        blit = True)
    if outputFilename != None:
        ani.save(outputFilename)
    if show == True:
        plt.show()

def auto_correlation(data: list) -> np.ndarray:
    ''' Performs the autocorrelation and normalizes 
    Returns
    -------
    numpy array
        normalized autocorrelation
    '''
    acor = np.correlate(data, data, mode = 'full')[len(data)-1:]
    nacor = acor / np.amax(acor)
    return nacor

def scatter_plot(N: list, t0: list, tau: list, amp: list, offset: list, k: list, 
        dt: list, chisqr: list) -> None:
    ''' Draws the correlation matrix and the scatter plot for the fitting parameters '''
    data = pandas.DataFrame({'N': N, 't0': t0, 'tau': tau, 'amp': amp, 
        'offset': offset, 'k': k, 'dt': dt, 'bic': chisqr})
    pandas.plotting.scatter_matrix(data)
    names = ['N', 't0', 'tau', 'amp', 'offset', 'k', 'dt', 'bic']
    correlations = data.corr()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(correlations, vmin = -1, vmax = 1)
    fig.colorbar(cax)
    ticks = np.arange(0,7,1)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xticklabels(names)
    ax.set_yticklabels(names)
    plt.show()

def heat_map(data: list, header) -> None:
    ''' Draw a heat map with the mean of all xy channels available.'''
    t = len(header)
    image = [np.zeros((64, 48)) for _ in range(t)]
    if t < 6:
        for pixel in data:
            for i in range(t):
                if i == 0:  
                    image[i][int(pixel[3])][int(pixel[4])] += 1
                elif i == 2:
                    image[i][int(pixel[3])][int(pixel[4])] = pixel[6] \
                    if pixel[6] > image[i][int(pixel[3])][int(pixel[4])] \
                    else image[i][int(pixel[3])][int(pixel[4])]
                else:
                    image[i][int(pixel[3])][int(pixel[4])] += pixel[i+4]
    else:
        for pixel in data:
            for i in range(t):
                if i == 0:
                    image[i][int(pixel[2][3])][int(pixel[2][4])] += 1
                elif i == 2:
                    image[i][int(pixel[2][3])][int(pixel[2][4])] = pixel[2][6] \
                    if pixel[2][6] > image[i][int(pixel[2][3])][int(pixel[2][4])] \
                    else image[i][int(pixel[2][3])][int(pixel[2][4])]
                elif i > 4 and i < 12:
                    image[i][int(pixel[2][3])][int(pixel[2][4])] += \
                        pixel[0].best_values[header[i]]
                elif i == 12:
                    image[i][int(pixel[2][3])][int(pixel[2][4])] += pixel[0].chisqr
                elif i == 13:
                    image[i][int(pixel[2][3])][int(pixel[2][4])] += pixel[0].bic
                else:
                    image[i][int(pixel[2][3])][int(pixel[2][4])] += pixel[2][i+4]
    for i in range(1,t):
        if i != 2:
            image[i] = np.divide(image[i], image[0], out=np.zeros_like(image[i]), 
                where=image[0] != 0)
    hzdict = dict(zip(header,image))
    fig, ax = plt.subplots(1,1)
    div = make_axes_locatable(ax)
    cax = div.append_axes('right', '5%', '5%')
    im = ax.imshow(image[0])
    cb = fig.colorbar(im, cax= cax)
    rax = plt.axes([0.03, 0.25, 0.15, 0.4], facecolor= 'lightgoldenrodyellow')
    radio = RadioButtons(rax, header)
    def hzfunc(label):
        #ax.clear()
        #mat = ax.imshow(hzdict[label])
        im.set_data(hzdict[label])
        im.set_clim(vmin= np.min((hzdict[label])), vmax= np.max((hzdict[label])))
        cb.draw_all()
        plt.draw()
    radio.on_clicked(hzfunc)
    plt.show()

def max_values(data: list):
    d = np.array(data)
    return d.max(axis = 1) - d[:,:4].mean()

def get_acf(data: list) -> list:
    size = int((len(data[0]) - 9)/ 2)
    acor = []
    for i in data:
        sig = i[9: 9 + size]
        acor.append(np.correlate(sig, sig, mode = 'full')[len(sig)-1:] / np.dot(sig,sig))
    acor = np.array(acor)
    return np.mean(acor, axis = 1)

def cost_function(pars, a1: float, a2: float, a3: float, a0: float, rt: float, ta: float, 
        ft: float, acf: list, S: list, W: list):
    vals = pars.valuesdict()
    c = list(vals.values())
    s0 = 0; s1 = 0; s2 = 0; s2a = 0 ;s3 = 0 
    s0 = np.sum(c)**2                           # Mean zero
    s1 = np.sum(c * np.arange(0, len(c)))**2    # ramp immunity
    fir = np.correlate(S, c, 'same')
    s2 = np.sum(np.power(fir[ta+rt:ta+rt+ft] - W[ta+rt:ta+rt+ft],2))
    for i in range(len(c)):                     # Signal to Noise ratio optimization
        for j in range(len(c)):
            s3 +=c[i]*c[j]*acf[abs(i-j)]
    for j in range(rt, rt + ft - 1):            # Flat-top restriction
        s2 += s2a**2
        for i in range(len(c)):
            s2a += c[i] * S[len(c)+j-i] - 1
    return a0*s0 + a1*s1 + a2*s2 + a3*s3

def W(x, rt: int, zt: int, ft: int): # output expected shape
    W = np.zeros(np.shape(x))
    for t in x:
        if t <= zt:
            W[t] = 0 
        elif t <= zt + rt:
            W[t] = (t - zt)*(1/rt)
        elif t < zt + rt + ft:
            W[t] = 1
        elif t < zt + 2*rt + ft: 
            W[t] = 1 + (zt + rt + ft - t)*(1/rt)
        else:
            W[t] = 0
    return W

def filter_optimization(coeff: dict, params_args:dict, model_params: dict) :
    ''' DPLMS optimization for FIR filter
    Takes the best parameters of the fitting and the noise autocorrelation function 
    to optimize the parameters of the filter using penalizied least means squares
    best_params = [t0, amp, tau, offset, k, dt, chisqr]
    '''
    model = lmfit.Model(crrcn)
    params_args['S'] = model.eval(x=np.arange(params_args.length), 
        params= make_parameters(model, model_params))
    method = params_args.pop('method', 'leastsq')
    params_args['W'] = params_args.pop('Wamp') * W(np.arange(params_args.length), params_args['rt'], 
        params_args.pop('zt') + params_args['ta'], params_args['ft'])
    p_true = lmfit.Parameters()
    for k, v in coeff.items():
        p_true.add(k, value= v, min= -1, max= 1)
    fir_obj = lmfit.minimize(cost_function, p_true, kws= params_args, method= method)
    res = []
    for _, param in fir_obj.params.items():
        res.append(param.value)
    return fir_obj, res, np.correlate(params_args['S'], res,'same')