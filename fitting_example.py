import numpy as np
import math
from scipy import optimize
import matplotlib.pyplot as plt

_math_model = textwrap.dedent('''\
def crrcn(x, {p}):
    crrcn = np.zeros(np.shape(x))
    for i, t in enumerate(x):
        if t < t0:
            crrcn[i] = offset
        elif t0 <= t and t < t0 + dt:
            crrcn[i] = offset + amp * (math.exp(N) / (N ** N)) * ((t - t0) / tau) ** N * math.exp( - (t - t0) / tau) 
        elif t0 + dt <= t: 
            crrcn[i] = offset + amp * (math.exp(N) / (N ** N)) * ((t - t0) / tau) ** N * math.exp( - (t - t0) / tau) \
                + k * amp * (math.exp(N) / (N ** N)) * ((t - (t0 + dt)) / tau) ** N * math.exp( - (t - (t0+ dt)) / tau)
    return crrcn
''')

def make_model(**kwargs):
    params = set(('t0', 'amp', 'tau', 'offset', 'k','dt')).difference(kwargs.keys())
    exec(_math_model.format(p = ','.join(params)), globals())
    return crrcn

def auto_correlation(data):
    acor = np.correlate(data, data, mode = 'full')[len(data)-1:]
    nacor = acor / np.amax(acor)
    return acor, nacor

def math_model(n):
    def crrn(x, t0, amp, tau, offset):
        #p[t0_, a_, n_, tau_, offset_]
        crrn = np.zeros(np.shape(x))
        for i,a in enumerate(x):
            if a < t0:
                crrn[i] = offset
            else: 
                crrn[i] = offset + amp * (math.exp(n) / (n ** n)) * ((a - t0) / tau) ** n * math.exp( - (a - t0) / tau)
        return crrn
    return crrn

def fitting_model (d, X, p) :
    model = math_model(8)
    errfunc = lambda p, x, y: model(x, *p) - y # Distance to the target function
    p1, success = optimize.leastsq(errfunc, p, args=(X, d))
    # Residual
    residuo = model(X, *p1) - y
    _, auto_co = auto_correlation(residuo)
    return p1, residuo, auto_co, residuo @ residuo
#errfunc = lambda params: np.sum(model(X, params) - d)
#p = optimize.minimize(errfunc, x0 = para   ms[1:])
y = [4.893750000000000000e+01,5.000000000000000000e+01,4.993750000000000000e+01,5.000000000000000000e+01,4.993750000000000000e+01,4.900000000000000000e+01,5.093750000000000000e+01,5.200000000000000000e+01,5.393750000000000000e+01,5.600000000000000000e+01,5.693750000000000000e+01,5.800000000000000000e+01,5.993750000000000000e+01,6.100000000000000000e+01,6.193750000000000000e+01,6.000000000000000000e+01,5.993750000000000000e+01,5.800000000000000000e+01,5.593750000000000000e+01,5.500000000000000000e+01,5.293750000000000000e+01,5.300000000000000000e+01,5.093750000000000000e+01,5.100000000000000000e+01,4.993750000000000000e+01,4.900000000000000000e+01,4.893750000000000000e+01,4.900000000000000000e+01,4.793750000000000000e+01,4.900000000000000000e+01,4.893750000000000000e+01,4.800000000000000000e+01]
x = list(range(len(y)))
p = [4,12,2,47]
p, resudio, auto_co, chsqr = fitting_model (y, x, p) 
var = sum(value ** 2 for value in resudio) / len(resudio)
res_sq = var ** 0.5
print(p,chsqr, res_sq, var)
model = math_model(8)

fig, axes = plt.subplots(nrows=3, ncols=1)
ax, ax1, ax2 = axes.flatten()
ax.plot(x,y, label = 'Real data')
ax.plot(x,model(x,*p), label='Best Fit')

ax1.plot(x,resudio)

ax2.bar(x, auto_co, width = 0.2)
ax2.plot(x, auto_co)
ax2.grid(True)
fig.legend(loc="upper left")
fig.tight_layout()
plt.show()
#print(result.chisqr, result.redchi)


import numpy as np
import scipy.optimize as optimize
import textwrap

funcstr = textwrap.dedent('''\
def func(x, {p}):
    if x < t0:
        return 0
    elif t0 <= x  and x < t0 + dt:
        return 1 + a
    elif t0 + 0 <= x:
        return 1 + a + b
''')

def make_model2(**kwargs):
    params = set(('a','b','dt','t0')).difference(kwargs.keys())
    exec(funcstr.format(p = ','.join(params).replace()), globals())
    return func

func=make_model(a=3, b=1)
