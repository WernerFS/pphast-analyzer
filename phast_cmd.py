import cmd2, argparse, glob, gc, phast_functions, os, pathlib, sys, re
import numpy as np

import matplotlib, pickle
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

from datetime import datetime

loop = True
next_ = False

def _append_slash_if_dir(p): 
    ''' Adds / if the matched string is a directory'''
    if p and os.path.isdir(p) and p[-1] != os.sep:
        return p + os.sep
    else:
        return p

def bounds(s):
    try:
        max_v, min_v = map(float, s.split(','))
        return min_v, max_v
    except:
        raise argparse.ArgumentTypeError("Bounds must be min, max")

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def percentage(v):
    try:
        v = float(v)
    except:
        raise argparse.ArgumentTypeError('Numeric value expected.')
    if v is not None:
        if v < 1:
            return v
        else:
            return int(v)
    else:
        return None

def exclusion_list(v) -> list:
    v = re.split(r"[, ]+", v)
    if len(v) > 1:
        if len(v) % 2:
            raise argparse.ArgumentTypeError('Expected pairs of coordinates (x,y).')
        else:
            v = [int(x) for x in v]
            return list(zip(v[0::2], v[1::2]))
    else:
        filename = pathlib.Path(v[0])
        if filename.exists():
            return [tuple(l) for l in np.loadtxt(filename, dtype= int, delimiter= ',').tolist()]
        else:
            raise argparse.ArgumentTypeError('Invalid filename, must be a comma separeted file.')
        return

def handle_close(event):
    global next_, loop
    loop = next_

def next_plot(event):
    global next_
    next_ = True
    plt.close()
class phast_cmd(cmd2.Cmd):
    def __init__(self):
        history_file = 'persistent_history.cmd2'
        super().__init__(use_ipython=True, persistent_history_file=history_file)
        self.self_in_py = True
        self.prompt = cmd2.style('pphast >: ', fg=cmd2.fg.blue, bold=True)
        self.intro =  cmd2.style(
            'This CLI application is intended to work with PPHAST processed data. Use -h or --help for more information.', 
            fg=cmd2.fg.green, bg=cmd2.bg.black, bold=True)
        self.allow_style = cmd2.ansi.STYLE_TERMINAL
        cmd2.Cmd.__init__(self)
        # Set the default category name
        self.default_category = 'cmd2 Built-in Commands'
        self.runs = []          # holds all the data in a list of numpy arrays
        self.noise = []
        self.acf = []           # signal autocorrelation mean
        self.residuals_ac = []
        self.model = 0
        self.best_params = []
        self.fit_acf = []
        self.fit = []
        self.fir = []         # filter coefficients 

    open_file_parser = argparse.ArgumentParser()
    open_file_parser.add_argument('filepath', type = str,
        help = 'Specify the filepath of the file to open.')
    open_file_parser.add_argument('-s', '--style', type = int, choices = [0,1], default = 0,
        help = 'Specify the format style of the file to open.')
    open_file_parser.add_argument('-n', '--noise', action = 'store_true', 
        help = 'Load noise file.')
    open_file_parser.add_argument('-c', '--cache', action = 'store_true', 
        help = 'Create a cache.')
    open_file_parser.add_argument('-x', '--max_values', action = 'store_true', 
        help = 'Plot a histogram of the maximum values.')
    open_file_parser.add_argument('-o', '--overlay', action = 'store_true', 
        help = 'Create an overlay of all the input data.')
    @cmd2.with_argparser(open_file_parser)
    def do_open_file(self, args):
        '''Opens a raw data file with different format from csv. Two file formats are considered:
        -s 0:
            [data] ======>  Run # Spill # Event-in-Spill # Cell X, Y
        -s 1:
            Flagged event #  Spill # Event-in-Spill # Cell X, Y
            Full ADC 32 bit stream for N bits 34
            [data]
        '''
        if args.noise:
            self.noise = phast_functions.process_file(args.filepath, args.style, args.cache)
            self.poutput('Data processed:' + str(len(self.runs)))
        else:
            self.runs += phast_functions.process_file(args.filepath, args.style, args.cache)
            if self.runs:
                self.poutput('Data processed:' + str(len(self.runs)))
            else:
                self.perror('Invalid Filename.')
                return
        if args.max_values:
            plt.hist(np.array(self.runs)[:,6], bins = 100, color = '#0504aa', log=True)
            plt.grid(True)
            plt.title("Max Values Histogram")
            plt.show()
        if args.overlay:
            fig, ax = plt.subplots()
            for l in self.runs:
                ax.plot(l[8:40])
            #plt.show()
            plt.savefig('overlay.png')
        return

    def complete_open_file(self, text, line, begidx, endidx):
        '''Uses TAB to autocomplete.
        Must be added to the function to autocomplete with the following line:
        complete_FUNCTION_NAME = complete_open_file
        '''
        before_arg = line.rfind(" ", 0, begidx)
        if before_arg == -1:
            return # arg not found
        fixed = line[before_arg + 1:begidx]  # fixed portion of the arg
        arg = line[before_arg + 1:endidx]
        pattern = arg + '*'
        completions = []
        for path in glob.glob(pattern):
            path = _append_slash_if_dir(path)
            completions.append(path.replace(fixed, "", 1))
        return completions

    open_cache_parser = argparse.ArgumentParser()
    open_cache_parser.add_argument('filepath', metavar = 'filename.cache' ,type = str, 
        help = 'Specify the filepath of the file to open (.cache).')
    open_cache_parser.add_argument('-x', '--max_values', action = 'store_true', 
        help = 'Plot a histogram of the maximum values.')
    open_cache_parser.add_argument('-o', '--overlay', action = 'store_true', 
        help = 'Create an overlay of all the input data.')
    @cmd2.with_argparser(open_cache_parser)
    def do_open_cache(self, args):
        '''
        Load a cache of previously processed data.
        '''
        self.runs += phast_functions.load_cache(args.filepath)
        self.poutput('Data processed count:' + str(len(self.runs)))
        if args.max_values:
            plt.hist(np.array(self.runs)[:,6], bins = 100, color = '#0504aa')
            plt.grid(True)
            plt.title("Max Values Histogram")
            plt.show()
        if args.overlay:
            fig, ax = plt.subplots()
            for l in self.runs:
                ax.plot(l[8:40])
            #plt.show()
            plt.savefig('overlay.png')
        return

    complete_open_cache = complete_open_file

    load_csv = argparse.ArgumentParser()
    load_csv.add_argument('filepath', metavar = 'filename.csv' ,type = str, 
        help = 'Specify the filepath of the file to open (.csv).')
    load_csv.add_argument('-x', '--max_values', action = 'store_true',
        help = 'Plot a histogram of the maximum values.')
    load_csv.add_argument('-o', '--overlay', action = 'store_true', 
        help = 'Create an overlay of all the input data.')
    @cmd2.with_argparser(open_cache_parser)
    def do_load_csv(self, args):
        '''
        Load a CSV file.
        File must follow the following format:
            [run#, spill#, event#, x, y, barycenter, max_value],[mean, stdev],[data * 32],[std_data * 32]
        '''
        self.runs = np.loadtxt(open(args.filepath, "rb"), delimiter=",")
        self.poutput('Data processed count:' + str(len(self.runs)))
        self.poutput('Column count:' + str(len(self.runs[0])))
        if args.max_values:
            plt.hist(np.array(self.runs)[:,6], bins = 100, color = '#0504aa')
            plt.grid(True)
            plt.title("Max Values Histogram")
            plt.show()
        if args.overlay:
            fig, ax = plt.subplots()
            for l in self.runs:
                ax.plot(l[8:40])
            #plt.show()
            plt.savefig('overlay.png')
        return

    complete_load_csv = complete_open_file

    filter_parser = argparse.ArgumentParser()
    filter_parser.add_argument('-b', '--bary', type = float, nargs = 2, metavar = ('min','max'), 
        help = 'Specify the range for the barycenter.')
    filter_parser.add_argument('-m', '--mean', type = float, 
        help = 'Specify the threshold for the mean rule.')
    filter_parser.add_argument('-e', '--exclude', type = exclusion_list, nargs='?',
        metavar = ('X,Y or filename'),
        help = 'Indicate a cell to be excluded or load a file with cells a list of cells to exclude.')
    filter_parser.add_argument('-p', '--plot', action = 'store_true', 
        help = 'Plot each result.')
    filter_parser.add_argument('-f', '--file', action = 'store_true', 
        help = 'Save results in file.')
    filter_parser.add_argument('-x', '--max_values', action = 'store_true',
        help = 'Plot a histogram of the maximum values.')
    filter_parser.add_argument('-o', '--overlay', action = 'store_true', 
        help = 'Create an overlay of all the input data.')
    @cmd2.with_argparser(filter_parser)
    def do_filter_data(self, args):
        '''
        Filters loaded data based on user supplied parameters.
        Allows the user to save the data on a csv file for later use.
        '''
        global next_, loop
        loop = True
        if len(self.runs) == 0:
            self.perror('No data loaded.')
            return
        self.runs = phast_functions.filter(self.runs, args) 
        self.poutput(f'Filtered data: {len(self.runs)}')
        if args.plot:
            i = 1
            for l in self.runs:
                next_ = False
                fig = plt.figure()
                phast_functions.get_plot(fig, l)
                plt.figtext(0.5, 0.1,f"{i}/{len(self.runs)}")
                axCloseButton = plt.axes([0.6, 0.1, 0.2, 0.06])
                bClose = matplotlib.widgets.Button(axCloseButton, "Next", hovercolor = '0.975')
                bClose.on_clicked(next_plot)
                fig.canvas.mpl_connect('close_event', handle_close)
                plt.show()
                i+=1
                if not loop:
                    break
        if args.max_values:
            plt.hist(np.array(self.runs)[:,6], bins = 100, color = '#0504aa', log=True)
            plt.grid(True)
            plt.title("Max Values Histogram")
            plt.xlabel('ADC Units')
            plt.show()
        if args.overlay:
            fig, ax = plt.subplots()
            for l in self.runs:
                ax.plot(l[8:40])
                ax.set_ylabel('ADC Units')
                ax.set_xlabel('Sample Time Periods')
            #plt.show()
            plt.savefig('overlay.png')
        if args.file:
            np.savetxt(
                ('filter' + '_'.join(list(map(str, list(vars(args).values())[4:]))) + '.csv')
                .replace(' ','').replace('-',''), self.runs, delimiter = ",")
        return

    complete_filter_data = complete_open_file

    data_parser = argparse.ArgumentParser()
    data_parser.add_argument('-r', '--run', type = float, help = 'Specify the run.')
    data_parser.add_argument('-s', '--spill', type = float, help = 'Specify the spill.')
    data_parser.add_argument('-e', '--event', type = float, help = 'Specify the event.')
    data_parser.add_argument('-c', '--cell', type = float, nargs = 2, metavar = ('X','Y'),
        help = 'Specify the cell X Y.')
    data_parser.add_argument('-se', '--segment', type = float, nargs = 4, metavar = ('X0','Y0','X1','Y1'),
        help = 'Specify the starting and ending cells to draw a rectangular segment to select from.')
    data_parser.add_argument('-p', '--plot', action = 'store_true', help = 'Plot each result.')
    data_parser.add_argument('-f', '--file', action = 'store_true', help = 'Save results in file.')
    #data_parser.add_argument('-s', '--source', type = int, choices = [0,1,2], default = 0,
    #    help = 'Specify the data source to draw the heat map from. 0 runs \n 1 fit \n 2 noise')
    data_parser.add_argument('-x', '--max_values', action = 'store_true', help = 'Plot a histogram of the maximum values.')
    data_parser.add_argument('-o', '--overlay', action = 'store_true', 
        help = 'Create an overlay of all the input data.')
    @cmd2.with_argparser(data_parser)
    def do_get_data(self, args):
        '''
        Searchs in loaded data the indexes specified by the user (does not works with fitted data or noise data).
        '''
        global loop, next_
        loop = True
        if len(self.runs) == 0:
            self.perror('No data loaded.')
            return
        self.runs = phast_functions.search(self.runs, args) # overwrites run, if search returns 0 data needs to be reloaded, needs fix
        self.poutput(f'Requested data: {len(self.runs)}')
        if args.plot and loop:
            i = 1
            for l in self.runs:
                next_ = False
                fig = plt.figure()
                phast_functions.get_plot(fig, l)
                plt.figtext(0.5, 0.1,f"{i}/{len(self.runs)}")
                axCloseButton = plt.axes([0.6, 0.1, 0.2, 0.06])
                bClose = matplotlib.widgets.Button(axCloseButton, "Next", hovercolor = '0.975')
                bClose.on_clicked(next_plot)
                fig.canvas.mpl_connect('close_event', handle_close)
                plt.show()
                i+=1
                if not loop:
                    break
        if args.max_values:
            plt.hist(np.array(self.runs)[:,6], bins = 100, color = '#0504aa')
            plt.grid(True)
            plt.title("Max Values Histogram")
            plt.xlabel('ADC Units')
            plt.show()
        if args.overlay:
            fig, ax = plt.subplots()
            for l in self.runs:
                ax.plot(l[8:40])
            #plt.show()
            plt.savefig('overlay.png')
        if args.file:
            np.savetxt(
                ('search_' + '_'.join(list(map(str, list(vars(args).values())[6:]))) + '.csv')
                .replace(' ','').replace('-', ''), self.runs, delimiter = ',')
        return

    fitting_parser = argparse.ArgumentParser()
    fitting_parser.add_argument('-f', '--file', type = str, 
        help = 'Specify the csv file to open.')
    fitting_parser.add_argument('-out', '--output', action = 'store_true', 
        help = 'Save results in csv file.')
    fitting_parser.add_argument('-pa', '--params', type = float, nargs = 7 ,
        default= [8, 4, 100, 1, 50, 0.3, 4],
        metavar = ('N', 't0', 'amp', 'tau', 'offset', 'k', 'dt'),
        help = 'Specify the initial parameters.')
    fitting_parser.add_argument('-b', '--bounds', type = bounds, nargs = 7 ,
        metavar = ('max,min'),
        default= [(-np.inf, np.inf)]*7,
        help = 'Specify the bounds for the parameters.')
    fitting_parser.add_argument('-v', '--vary', type = str2bool, nargs = 7 ,
        metavar = ('True/False'),
        default= [True]*7,
        help = 'Pass a mask to ignore some parameters and treat them as constants.')
    fitting_parser.add_argument('-m', '--method', type = str,
        default = 'leastsq',
        help = 'Method used for fitting. Deafult least mean squares.')
    fitting_parser.add_argument('-c', '--chisqr', type = percentage,
        default= None,
        help = 'Upper limit value to filter results by chisqr.')
    fitting_parser.add_argument('-r', '--result', action = 'store_true', 
        help = 'Plot average results and parameters statistics.')
    fitting_parser.add_argument('-p', '--plot', action = 'store_true', 
        help = 'Plot each result.')
    fitting_parser.add_argument('-w', '--weighted', action = 'store_true', 
        help = 'Calculate best parameters based on weighted mean using the chi square.')
    fitting_parser.add_argument('-s', '--scatter', action = 'store_true', 
        help = 'Plot correlation scatter plot.')
    fitting_parser.add_argument('-x', '--max_values', action = 'store_true',
        help = 'Plot a histogram of the maximum values.')
    fitting_parser.add_argument('-o', '--overlay', action = 'store_true', 
        help = 'Create an overlay of all the input data.')
    fitting_parser.add_argument('-k', '--krit', action='store_true')
    fitting_parser.add_argument('-t', '--time', action='store_true',
        help = 'Calculate t0 via an alternative method and plot the histogram of fitted t0 and calculated.')
    @cmd2.with_argparser(fitting_parser)
    def do_fit_data(self, args):
        '''
        Fits loaded data according to user specified initial parameters.
        The mathematical model for fitting is based on a double semigaussian pulse.
        '''
        global loop, next_
        loop = True
        pars_name= ['N', 't0', 'amp', 'tau', 'offset', 'k', 'dt']
        if len(self.runs) == 0 and not args.file:
            self.perror('No data loaded or csv file to load specified. Use -h for help')
            return
        if args.file:
            filename = pathlib.Path(args.file)
            if not filename.exists():
                self.perror('Oops, file ' + str(filename) + ' not found.')
                return
            self.poutput('Yay, the file exists!')
            self.poutput('Ready to process: ' + filename.name)
            self.runs = np.loadtxt(open(filename, 'rb'), delimiter = ',')
            self.poutput('Process: {} rows {} columns'.format(len(self.runs), len(self.runs[0])))
        params = dict(zip(pars_name,args.params))
        params['chisqr'] = args.chisqr
        params['method'] = args.method
        bounds = dict(zip(pars_name,args.bounds))
        mask = dict(zip(pars_name,args.vary))
        self.poutput("Fitting data, please wait.")
        self.fit, self.model = phast_functions.fitting(self.runs, params, bounds, mask)
        if self.fit == None:
            return
        total_fits = len(self.fit)
        self.poutput("Processing results.")
        self.poutput(f"Successfully fitted {total_fits}")
        if total_fits == 0 :
            self.perror("Try changing the initial parameters, bounds or method.")
            return
        if type(args.chisqr) == float:
            self.fit.sort(key = lambda x:x[0].chisqr, reverse= True)
            cut = int(total_fits * args.chisqr)
            self.poutput(f"Taking best {args.chisqr * 100}%.")
            self.fit = self.fit[cut:]
        x = np.arange(0, len(self.fit[0][0].data))
        N =[]; t0 = []; amp = []; tau = []; offset = []; k = []; dt = [] # all fitting params
        max_vals = []
        chisqr = []; residuals = []; self.residuals_ac = []
        redchi = []; bic= []; aic = []; dl = []
        t0_alt = []; t0_err = []; data = []
        i = 1
        for result, residual_ac, d, t0_alt_i in self.fit:
            if args.plot and loop:
                next_ = False
                fig = plt.figure(figsize=(12.8,4.8))
                fig.suptitle(f'X:{d[3]}, Y:{d[4]}')
                result.plot(show_init= True, fig= fig, xlabel='Sample Time Periods', ylabel='ADC Units')
                plt.text(len(x) + 1,min(result.data),result.fit_report(show_correl= False))
                plt.figtext(0.5, 0.1,f"{i}/{len(self.runs)}")
                axCloseButton = plt.axes([0.7, 0.8, 0.2, 0.06])
                bClose = matplotlib.widgets.Button(axCloseButton, "Next", hovercolor = '0.975')
                bClose.on_clicked(next_plot)
                fig.canvas.mpl_connect('close_event', handle_close)
                fig.tight_layout()
                plt.show()
                i+=1
            N.append(result.best_values['N'])
            t0.append(result.best_values['t0'])
            amp.append(result.best_values['amp'])
            tau.append(result.best_values['tau'])
            offset.append(result.best_values['offset'])
            k.append(result.best_values['k'])
            dt.append(result.best_values['dt'])
            dl.append(d) # recover metadata, should change var name
            data.append(result.data)
            max_vals.append(d[6])
            if args.time:
                t0_alt.append(t0_alt_i)
                t0_err.append(abs(t0_alt_i - t0[-1]))
                t0alt_avr = np.mean(t0_alt)
            chisqr.append(result.chisqr)
            bic.append(result.bic)
            if args.krit :
                redchi.append(result.redchi); aic.append(result.aic)
            residuals.append(result.residual)
            self.residuals_ac.append(residual_ac)
        if args.krit:
            self.poutput(f"Accumulated Chisqr {np.sum(chisqr)}")
            self.poutput(f"Averaged Chisqr {np.mean(chisqr)}")
            self.poutput(f"Accumulated RedChisqr {np.sum(redchi)}")
            self.poutput(f"Averaged RedChisqr {np.mean(redchi)}")
            self.poutput(f"Accumulated Bic {np.sum(bic)}")
            self.poutput(f"Averaged Bic {np.mean(bic)}")
            self.poutput(f"Accumulated Aic {np.sum(aic)}")
            self.poutput(f"Averaged Aic {np.mean(aic)}")
            fig, auxax = plt.subplots()
            auxax.plot(*zip(*sorted(zip(amp, chisqr))), 'bo', label= 'Chisqr')
            auxax.plot(*zip(*sorted(zip(amp, redchi))), 'r+', label = 'Reduced Chisqr')
            auxax.plot(*zip(*sorted(zip(amp, bic))), 'g^',  label= 'BIC')
            auxax.plot(*zip(*sorted(zip(amp, aic))), 'y*', label = 'AIC')
            auxax.set_ylabel('Criteria Value')
            auxax.set_xlabel('Amplitude (ADC Counts)')
            fig.suptitle('Goodness of Fit')
            plt.legend(loc='upper right')
            plt.show()

        bin_size = int(40 * np.tanh(0.02 * (len(t0) - 150)) + 60)
        t0_avr = np.mean(t0)
        if args.time and args.plot:
            fig, axes = plt.subplots(2)
            axes[0].hist(t0, bins = bin_size, alpha=0.5, label='Fitted')
            axes[0].hist(t0_alt, bins = bin_size, alpha=0.5, label='Calculated')
            axes[0].legend(loc='upper right')
            axes[0].set(xlabel='Sampling time periods', ylabel='Counts')
            axes[0].set_title('Histogram of Values')
            #for a, b in zip(t0,t0_alt):
            #    t0_err.append(np.abs((a - b )))
            axes[1].hist(t0_err, bins = bin_size)
            axes[1].set(xlabel='Sampling time periods', ylabel='Counts')
            axes[1].set_title('Histogram of Differences')
            fig.suptitle('Time of Arrival Comparisson')
            plt.show()
        if args.weighted:
            title = 'Best Model  weighted '
            self.best_params = {'N': np.average(N, weights = chisqr),
               't0': np.average(t0, weights = chisqr),
               'amp': np.average(amp, weights = chisqr),
               'tau': np.average(tau, weights= chisqr),
               'offset': np.average(offset, weights= chisqr),
               'k': np.average(k, weights= chisqr),
               'dt': np.average(dt, weights= chisqr)}
        else:
            title = 'Best Model '
            self.best_params = {'N':np.mean(N), 
                't0':t0_avr, 
                'amp':np.mean(amp), 
                'tau':np.mean(tau),
                'offset':np.mean(offset), 
                'k':np.mean(k), 
                'dt':np.mean(dt)}
        if args.output:
            if args.time:
                outputHeader = 'Run,Spill,Event,X,Y,Barycenter,peak_amp,Mean,Std,N,t0,t0_alt,amp,tau,offset,k,dt,bic,chisqr,' + \
                    ','.join(["{}{:02}".format(a_,b_) for a_,b_ in zip(['d']*32,range(32))]) + ',' + \
                    ','.join(["{}{:02}".format(a_,b_) for a_,b_ in zip(['r']*32,range(32))])
                outputData = np.column_stack([dl, N, t0,t0_alt, amp, tau, offset, k, dt, bic, chisqr, data, residuals])
                np.savetxt('fit' + '_'+ datetime.now().strftime("%Y%m%d_%H%M%S") + '.csv', outputData, header= outputHeader, delimiter = ",")
            else:
                outputHeader = 'Run,Spill,Event,X,Y,Barycenter,peak_amp,Mean,Std,N,t0,amp,tau,offset,k,dt,bic,chisqr,' + \
                    ','.join(["{}{:02}".format(a_,b_) for a_,b_ in zip(['d']*32,range(32))]) + ',' + \
                    ','.join(["{}{:02}".format(a_,b_) for a_,b_ in zip(['r']*32,range(32))])
                outputData = np.column_stack([dl, N, t0, amp, tau, offset, k, dt, bic, chisqr, data, residuals])
                np.savetxt('fit' + '_'+ datetime.now().strftime("%Y%m%d_%H%M%S") + '.csv', outputData, header= outputHeader, delimiter = ",")
        residuals = np.mean(np.array(residuals), 0)
        self.residuals_ac = np.mean(np.array(self.residuals_ac), 0)
        self.poutput("Best Parameters")
        self.poutput(self.best_params) 
        if args.result:
            try:
                fig, axes = plt.subplots(nrows = 3, ncols = 4, figsize = (3*4, 3*3))
                ax, ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11 = axes.flatten()
                ax.plot(x, self.model.eval(x=x, params= phast_functions.make_parameters(self.model, self.best_params, bounds, mask)), label='Fit')
                ax.grid(True)
                ax.set_xlabel('Sample Time Periods')
                ax.set_ylabel('ADC Units')
                ax.set_title(title + args.method)

                ax2.hist(t0, bins = bin_size, color ='#0504aa')
                ax2.grid(True)
                ax2.set_xlabel('Sample Time Periods')
                ax2.set_ylabel('Counts')
                ax2.set_title(f"T0, Mean: {self.best_params['t0']:.2f}")

                ax3.hist(amp, bins = bin_size, color = '#0504aa')
                ax3.grid(True)
                ax3.set_xlabel('ADC Units')
                ax3.set_ylabel('Counts')
                ax3.set_title(f"Amp, Mean: {self.best_params['amp']:.2f}")

                ax1.plot(x, residuals, 'bo', linewidth = 1.0) 
                ax1.grid(True)
                ax1.set_xlabel('Sample Time Periods')
                ax1.set_ylabel('ADC Units')
                ax1.set_title(f"Residuals Average")

                ax4.hist(tau, bins = bin_size, color ='#0504aa')
                ax4.grid(True)
                ax4.set_xlabel('Seconds')
                ax4.set_ylabel('Counts')
                ax4.set_title(f"Tau, Mean: {self.best_params['tau']:.2f}")

                ax7.hist(offset, bins = bin_size, color = '#0504aa')
                ax7.grid(True)
                ax7.set_xlabel('ADC Units')
                ax7.set_ylabel('Counts')
                ax7.set_title(f"Offset, Mean: {self.best_params['offset']:.2f}")

                ax6.plot(x, self.residuals_ac, 'bo', linewidth = 1.0) 
                ax6.grid(True)
                ax6.set_xlabel('Sample Time Periods')
                ax6.set_ylabel('ADC Units')
                ax6.set_title(f"Residuals Average Autocorrelation")

                ax5.hist(chisqr, bins = bin_size, color = '#0504aa')
                ax5.grid(True)
                ax5.set_ylabel('Counts')
                ax5.set_title(f"Chisqr {np.mean(chisqr):.2f}")
                ax5.set_yscale("symlog")

                ax8.hist(k, bins = bin_size, color = '#0504aa')
                ax8.grid(True)
                ax8.set_xlabel('Amplitude Percentage')
                ax8.set_ylabel('Counts')
                ax8.set_title(f"K, Mean: {self.best_params['k']:.2f}")

                ax9.hist(dt, bins = bin_size, color = '#0504aa')
                ax9.grid(True)
                ax9.set_xlabel('Sample Time Periods')
                ax9.set_ylabel('Counts')
                ax9.set_title(f"Time Delay, Mean: {self.best_params['dt']:.2f}")
                
                ax10.hist(N, bins = bin_size, color = '#0504aa')
                ax10.grid(True)
                ax10.set_title(f"N, Mean: {self.best_params['N']:.2f}")
                
                ax11.axis('off')

                plt.tight_layout()
                plt.show()
                plt.close('all')
            except KeyboardInterrupt:
                return
        if args.scatter:
            phast_functions.scatter_plot(N, t0, tau, amp, offset, k, dt, bic)
        if args.max_values:
            plt.hist([x[-1][-1] for x in self.fit], bins = 100, color = '#0504aa')
            plt.grid(True)
            plt.xlabel('ADC Units')
            plt.ylabel('Counts')
            plt.title("Max Values Histogram")
            plt.show()
        return

    complete_fit_data = complete_open_file

    acf_parser = argparse.ArgumentParser()
    acf_parser.add_argument('-o', '--output', action = 'store_true', 
        help = 'Save results in csv file.')
    acf_parser.add_argument('-p', '--plot', action = 'store_true', 
        help = 'Plot result.')
    acf_parser.add_argument('-v', '--verbose', action = 'store_true', 
        help = 'Print statistics.')
    @cmd2.with_argparser(acf_parser)
    def do_noise_ACF(self, args):
        '''get the average of the autocorrelation functions from file and save it in the self.ACF array
        dump data after ACF performed,
        calculation on self.noise'''
        if len(self.noise) == 0:
            self.perror('No noise data loaded.')
            return
        self.acf = phast_functions.get_acf(self.noise)
        if args.verbose:
            self.poutput(f'Average of all autocorrelation: {self.acf}')
        pass

    def do_current_data(self, args):
        '''Prints the size of the data currently loaded. Useful when weird error messages appear'''
        if len(self.runs) != 0:
            self.poutput(f'Loaded data: {len(self.runs)}')
        else:
            self.perror('No loaded data.')
        if len(self.fit) != 0:
            self.poutput(f'Fitted data: {len(self.fit)}')
        else:
            self.perror('No fitted data.')

    heat_map_parser = argparse.ArgumentParser()
    heat_map_parser.add_argument('-s', '--source', type = int, choices = [0,1,2], default = 0,
        help = 'Specify the data source to draw the heat map from. 0 runs \n 1 fit \n 2 noise')
    @cmd2.with_argparser(heat_map_parser)
    def do_get_heat_map(self, args):
        '''Creates a heat map of the loaded data taking the xy coordinates.'''
        headers = ['Frequency','Barycenter', 'Max', 'Mean', 'Std']
        if args.source == 0:
            if len(self.runs) == 0:
                self.perror('No data loaded.')
            else:
                phast_functions.heat_map(self.runs, headers)
        elif args.source == 1:
            if len(self.fit) == 0:
                self.perror('No fits data found.')
                return
            else:
                headers.extend(['N', 't0', 'amp', 'tau', 'offset', 'k', 'dt', 'chisqr', 'bic'])
                phast_functions.heat_map(self.fit, headers)
        elif args.source == 2:
            if len(self.noise) == 0:
                self.perror('No noise data loaded.')
                return
            else:
                phast_functions.heat_map(self.noise, headers)
        return

    def do_clear_memory(self, args):
        self.runs = []
        gc.collect()
        
    def do_exit(self, args):
        '''
        Does this really need an explanaition?
        '''
        self.poutput("Closing")
        self.do_clear_memory(self)
        return True

    dplms_parser = argparse.ArgumentParser()
    dplms_parser.add_argument('-f', '--file', type = str, 
        help = 'Specify the csv file to open')
    dplms_parser.add_argument('-o', '--output', action = 'store_true', 
        help = 'Save results in csv file')
    dplms_parser.add_argument('-i', '--init', type = float, nargs ='+',
        help = 'Initial filter coefficients')
    dplms_parser.add_argument('-pa', '--params', type = float, nargs = 4, 
        metavar= ('rt','ft', 'zt', 'length'),
        help = 'Parameters for the trapezoid')
    dplms_parser.add_argument('-a', '--alphas', type = float, nargs = 4, 
        metavar= ('alpha1','alpha2','alpha3','alpha4'),
        help = 'Weights for all avaliable criteria to minimize')
    dplms_parser.add_argument('-mp', '--model_params', type = float, nargs = 7, 
        metavar = ('N', 't0', 'amp', 'tau', 'offset', 'k', 'dt'),
        help = 'Semigaussian pulse model parameters')
    dplms_parser.add_argument('-c', '--coeff', type = int, 
        help = 'Length of FIR, if not provided use --init to input the FIR.')
    dplms_parser.add_argument('-p', '--plot', action = 'store_true', 
        help = 'Plot coefficients.')
    dplms_parser.add_argument('-m', '--method', type = str,
        default = 'leastsq',
        help = 'Method used for fitting, the deafult is least mean squares')
    dplms_parser.add_argument('-n', '--noise', action = 'store_true', 
        help = 'Use the autocorrelation function of the noise_ACF function as input')
    dplms_parser.add_argument('-v', '--verbose', action = 'store_true', 
        help = 'Print full minimize report')
    @cmd2.with_argparser(dplms_parser)
    def do_dplms(self, args):
        '''get self.math_model, get self.ACF from another function which does not exists for now here,
        gets weights from user
        criteria:
            a0 offset agnostic sum of C = 0
            a1 flatop sum convolution = constant for flat top
            a2 minimize noise on output minimize variance
        how to load ACF and best_params?!
        '''
        if args.noise:
            if not self.acf:
                self.perror('No noise autocorrelation data present.')
                return
            acf = self.acf
        else:
            if not self.residuals_ac:
                self.perror('No residuals autocorrelation data present.')
                return
            acf = self.residuals_ac
        if args.init:
            args.coeff = len(args.init)
        else:
            args.init = [0]*args.coeff
        
        coeff = dict(zip([f'c{N}' for N in range(len(args.init))],args.init))
        
        alf_dict = dict(zip([f'a{N}' for N in range(4)],args.alpha))
        acf_dict = {'acf': acf}
        pars_dict = {'rt':args.params[0],'ft':args.params[1], 'zt': args.params[2] + args.model_params[1], 'length': args.params[3]}
        params_args = {**alf_dict, **acf_dict, **pars_dict}
        params_args['method'] = args.method
        pars_name= ['N', 't0', 'amp', 'tau', 'offset', 'k', 'dt']
        self.fir = phast_functions.filter_optimization(coeff, params_args, dict(zip(pars_name,args.model_params)))
        coef = self.fir[1]
        out = self.fir[2]
            
        if args.verbose:
            self.poutput(self.fir[0].fir_obj)
        else:
            self.poutput(coef)
        if args.plot:
            plt.plot(coef)
            plt.plot(out)
            plt.show()

    do_quit = do_exit

if __name__ == '__main__':
    prompt = phast_cmd()
    sys.exit(prompt.cmdloop())